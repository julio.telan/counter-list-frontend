import axios from "axios";

// Action Creators
function changeTextInput(value) {
    return {
        type: "CHANGE_TEXT_INPUT",
        value
    };
}

function receiveCounters(value) {
    return {
        type: "RECEIVE_COUNTERS",
        value
    };
}

//API Calls
function apiCall(method, data, url) {
    return (dispatch) => {
        return axios({
            url: url,
            headers: {
                "Content-Type": "application/json"
            },
            data: data,
            timeout: 20000,
            method: method,
            responseType: "json"
        })
            .then((response) => {
                dispatch(changeTextInput(""));
                dispatch(changeValidationState(null));
                dispatch(receiveCounters(response.data));
            })
            .catch((error) => {
                alert(error);
            });
    };
}
function fetchCounters() {
    return (dispatch) => {
        dispatch(apiCall("get", {}, '/api/v1/counters'));
    };
}

function createCounter() {
    return (dispatch, getState) => {
        if (getState().text_input.length < 1) {
            return null;
        } else {
            dispatch(apiCall("post", { title: getState().text_input }, '/api/v1/counter'));
        }
    };
}

function counterAction(type, id) {
    return (dispatch) => {
        dispatch(apiCall("post", { id }, `/api/v1/counter/${type}`));
    };
}

function deleteRow(id) {
    return (dispatch) => {
        dispatch(apiCall("delete", { id }, '/api/v1/counter'));
    };
}

function changeValidationState(value) {
    return {
        type: "CHANGE_VALIDATION_STATE",
        value
    }
}

// Public Functions
export function onChangeTextInput(e) {
    return (dispatch) => {
        dispatch(changeTextInput(e.target.value));
    };
}

export function onEnter(e) {
    return (dispatch) => {
        if (e.key === 'Enter') {
            dispatch(createCounter());
        }
    }
}

export function fetchCountersNow() {
    return (dispatch) => {
        dispatch(fetchCounters());
    };
}

export function onClickCreateCounter() {
    return (dispatch) => {
        dispatch(createCounter());
    }
}

export function onClickCounterButton(type, id) {
    return (dispatch) => {
        dispatch(counterAction(type, id));
    }
}

export function onClickDeleteRow(id) {
    return (dispatch) => {
        let result = window.confirm("Are you sure you want to delete this?");
        if (result) {
            dispatch(deleteRow(id));
        }
    }
}

export function onBlurTextInput(e) {
    return (dispatch) => {
        if (e.target.value.length === 0) {
            dispatch(changeValidationState('error'));
        } else {
            dispatch(changeValidationState('success'));
        }
    }
}