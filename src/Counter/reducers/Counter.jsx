import { combineReducers } from "redux";

function counters(counters = [], action) {
    switch (action.type) {
    case "RECEIVE_COUNTERS":{
        return action.value;
    }
    default:
        return counters;
    }
}

function text_input(text_input = "", action) {
    switch(action.type) {
    case "CHANGE_TEXT_INPUT":
        return action.value;
    default:
        return text_input;
    }
}

function input_validation (input_validation = null, action) {
    switch(action.type) {
    case "CHANGE_VALIDATION_STATE":
        return action.value;
    default:
        return input_validation;
    }
}


const CounterReducer = combineReducers({
    counters,
    text_input,
    input_validation
});


export default CounterReducer;
