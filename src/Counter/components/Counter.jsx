import React from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";
import * as CounterActions from "../actions/Counter";
import { Button, Table, InputGroup, FormGroup } from "react-bootstrap";
import CounterRow from "./CounterRow";
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';

class Counter extends React.Component {
    componentWillMount() {
        this.props.fetchCountersNow();
    }

    componentDidUpdate() {
        this.focusDiv();
    }

    focusDiv() {
        ReactDOM.findDOMNode(this.refs.input).focus();
    }

    render(){
        let total_count = 0;
        let counter_row = this.props.counters.map((counter, i) => {
            total_count += counter.count;
            return <CounterRow counter={counter} 
                       i={i} key={"counter_row_" + i} 
                       onClickCounterButton={this.props.onClickCounterButton.bind(this)} 
                       onClickDeleteRow={this.props.onClickDeleteRow.bind(this)}
                    />
        });
        return (
            <div id="App">
                <div className="container card col-md-offset-3 col-md-6">
                    <h3 className="text-center">Count Me In!</h3>
                    <br />
                    <FormGroup
                        validationState={this.props.input_validation}
                    >
                        <InputGroup>
                            <input type="text" 
                                ref="input"
                                className="form-control" 
                                value={this.props.text_input}
                                onKeyPress={(e) => this.props.onEnter(e)}
                                placeholder="Type Counter Name Here"
                                onBlur={(e) => this.props.onBlurTextInput(e)}
                                onChange={(e) => this.props.onChangeTextInput(e)} />
                            <InputGroup.Button>
                                <Button 
                                    bsStyle="success" 
                                    onClick={() => this.props.onClickCreateCounter()}>
                                <FontAwesome
                                    name="user-plus"
                                    style={{"color": "#f0ede6"}}
                                 />
                                </Button>
                            </InputGroup.Button>
                        </InputGroup>
                    </FormGroup>
                    <Table hover>
                        <tbody>
                            {counter_row}
                        </tbody>
                        {this.props.counters.length !== 0 && <tfoot>
                            <tr>
                                <td className="text-right">
                                    <h5><strong>Total</strong></h5>
                                </td>
                                <td className="text-center">
                                    <h5><strong>{total_count}</strong></h5>
                                </td>
                            </tr>
                        </tfoot>}
                    </Table>
                </div>
            </div>
        );
    }
}

Counter.propTypes = {
    text_input: PropTypes.string.isRequired,
    counters: PropTypes.array.isRequired,
    onEnter: PropTypes.func.isRequired,
    onChangeTextInput: PropTypes.func.isRequired,
    onClickCreateCounter: PropTypes.func.isRequired,
    onClickCounterButton: PropTypes.func.isRequired,
    onClickDeleteRow: PropTypes.func.isRequired
};

function mapStateToProps(state) {
    return state;
}
const VisibleCounter = connect(
    mapStateToProps,
    CounterActions
)(Counter);

export default VisibleCounter;
