import React from "react";
import PropTypes from 'prop-types';
import FontAwesome from "react-fontawesome";

export default class CounterRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            i: props.i,
            counter: props.counter,
            onClickCounterButton: props.onClickCounterButton,
            onClickDeleteRow: props.onClickDeleteRow
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            i: nextProps.i,
            counter: nextProps.counter
        })
    }

    render() {
        return(
            <tr style={{"paddingBottom": "-10px"}} key={"counter_row_td_" + this.props.i}>
                <td className="col-md-10">
                    <div>
                        <div className="col-md-1 padding-left-0">
                            <FontAwesome
                                name="times"
                                style={{"color": "#ff0000a6", "paddingTop": "10px", "cursor": "pointer"}}
                                onClick={() => this.state.onClickDeleteRow(this.state.counter.id)}
                            />
                        </div>
                        <div className="col-md-10" style={{"marginLeft": "-25px"}}>
                            <h5>{this.state.counter.title}</h5>
                        </div>
                    </div>
                </td>
                <td className="col-md-2 text-center">
                    <FontAwesome
                        name="minus"
                        className="pull-left"
                        style={{"color": "#ff0000a6", "paddingTop": "5px", "cursor": "pointer"}}
                        onClick={() => this.state.onClickCounterButton("dec", this.props.counter.id)}
                    />
                    <span>{this.state.counter.count}</span>
                    <FontAwesome
                        name="plus"
                        className="pull-right"
                        style={{"color": "#5cb85c", "paddingTop": "5px", "cursor": "pointer"}}
                        onClick={() => this.state.onClickCounterButton("inc", this.props.counter.id)}
                    />
                </td>
            </tr>
        );
    }
}

CounterRow.propTypes = {
    i: PropTypes.number.isRequired,
    counter: PropTypes.object.isRequired,
    onClickCounterButton: PropTypes.func.isRequired,
    onClickDeleteRow: PropTypes.func.isRequired
};
