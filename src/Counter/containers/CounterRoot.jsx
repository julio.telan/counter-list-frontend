import { compose, createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunkMiddleware from "redux-thunk";
import VisibleCounter from "../components/Counter";
import React from "react";
import CounterReducer from "../reducers/Counter";

const createCounterStore = compose(
    applyMiddleware(thunkMiddleware)
)(createStore);

class CounterRoot extends React.Component {
    componentWillMount() {
        let initialState = {};
        this.store = createCounterStore(CounterReducer, initialState);
    }

    render() {
        return (
            <Provider store={this.store}>
                <VisibleCounter />
            </Provider>
        );
    }
}

CounterRoot.propTypes = {
};

export default CounterRoot;
