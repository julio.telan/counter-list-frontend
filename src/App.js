import React, { Component } from 'react';
import './App.css';
import VisibleCounterRoot from  './Counter/containers/CounterRoot';

class App extends Component {
  render() {
    return (
      <div className="container">
        <VisibleCounterRoot />
      </div>
    );
  }
}

export default App;
