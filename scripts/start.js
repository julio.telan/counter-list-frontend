'use strict';

// Do this as the first thing so that any code reading it knows the right env.
process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
  throw err;
});

// Ensure environment variables are read.
require('../config/env');

const fs = require('fs');
const chalk = require('chalk');
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const clearConsole = require('react-dev-utils/clearConsole');
const checkRequiredFiles = require('react-dev-utils/checkRequiredFiles');
const {
  choosePort,
  createCompiler,
  prepareProxy,
  prepareUrls,
} = require('react-dev-utils/WebpackDevServerUtils');
const openBrowser = require('react-dev-utils/openBrowser');
const paths = require('../config/paths');
const config = require('../config/webpack.config.dev');
const createDevServerConfig = require('../config/webpackDevServer.config');

const useYarn = fs.existsSync(paths.yarnLockFile);
const isInteractive = process.stdout.isTTY;

// Warn and crash if required files are missing
if (!checkRequiredFiles([paths.appHtml, paths.appIndexJs])) {
  process.exit(1);
}

// Tools like Cloud9 rely on this.
const DEFAULT_PORT = parseInt(process.env.PORT, 10) || 3000;
const HOST = process.env.HOST || '0.0.0.0';

if (process.env.HOST) {
  console.log(
    chalk.cyan(
      `Attempting to bind to HOST environment variable: ${chalk.yellow(
        chalk.bold(process.env.HOST)
      )}`
    )
  );
  console.log(
    `If this was unintentional, check that you haven't mistakenly set it in your shell.`
  );
  console.log(`Learn more here: ${chalk.yellow('http://bit.ly/2mwWSwH')}`);
  console.log();
}

// We attempt to use the default port but if it is busy, we offer the user to
// run on a different port. `choosePort()` Promise resolves to the next free port.
choosePort(HOST, DEFAULT_PORT)
  .then(port => {
    if (port == null) {
      // We have not found a port.
      return;
    }
    const protocol = process.env.HTTPS === 'true' ? 'https' : 'http';
    const appName = require(paths.appPackageJson).name;
    const urls = prepareUrls(protocol, HOST, port);
    // Create a webpack compiler that is configured with custom messages.
    const compiler = createCompiler(webpack, config, appName, urls, useYarn);
    // Load proxy config
    const proxySetting = require(paths.appPackageJson).proxy;
    const proxyConfig = prepareProxy(proxySetting, paths.appPublic);
    // Serve webpack assets generated by the compiler over a web sever.
    const serverConfig = createDevServerConfig(
      proxyConfig,
      urls.lanUrlForConfig
    );
    const devServer = new WebpackDevServer(compiler, serverConfig);
    // Launch WebpackDevServer.
    devServer.listen(port, HOST, err => {
      if (err) {
        return console.log(err);
      }
      if (isInteractive) {
        clearConsole();
      }
      console.log(chalk.cyan('Starting the development server...\n'));
      openBrowser(urls.localUrlForBrowser);
    });

    ['SIGINT', 'SIGTERM'].forEach(function(sig) {
      process.on(sig, function() {
        devServer.close();
        process.exit();
      });
    });
  })
  .catch(err => {
    if (err && err.message) {
      console.log(err.message);
    }
    process.exit(1);
  });


  var express     = require("express");
  var app         = express();
  var bodyParser  = require("body-parser");
  var compression = require("compression");
  var morgan      = require("morgan");
  var PORT        = Number( process.env.PORT || 3001 );
  var Counters    = require("../lib/Counters");
  
  app.use(morgan("combined"));
  app.use(bodyParser.urlencoded({extended: false}));
  app.use(bodyParser.json());
  app.use(compression());
  
  function sendFile(name) {
    return function(req, res) {
      res.sendFile(__dirname + "/static/" + name);
    };
  }
  
  app.get("/", sendFile("index.html"));
  app.get("/app.js", sendFile("app.js"));
  app.get("/Counter/containers/CounterRoot.js", sendFile("Counter/containers/CounterRoot.js"));
  app.get("/app.css", sendFile("app.css"));
  
  // [json] GET /api/v1/counters
  // => [
  // =>   {id: "asdf", title: "boop",  count: 4},
  // =>   {id: "zxcv", title: "steve", count: 3}
  // => ]
  app.get("/api/v1/counters", function(req, res) {
    res.json(Counters.all())
  });
  
  // [json] POST {title: "bob"} /api/v1/counters
  // => [
  // =>   {id: "asdf", title: "boop",  count: 4},
  // =>   {id: "zxcv", title: "steve", count: 3},
  // =>   {id: "qwer", title: "bob",   count: 0}
  // => ]
  app.post("/api/v1/counter", function(req, res) {
    res.json(Counters.create(req.body.title));
  })
  
  // [json] DELETE {id: "asdf"} /api/v1/counter
  // => [
  // =>   {id: "zxcv", title: "steve", count: 3},
  // =>   {id: "qwer", title: "bob",   count: 0}
  // => ]
  app.delete("/api/v1/counter", function(req, res) {
    res.json(Counters.delete(req.body.id));
  });
  
  // [json] POST {id: "qwer"} /api/v1/counter/inc
  // => [
  // =>   {id: "zxcv", title: "steve", count: 3},
  // =>   {id: "qwer", title: "bob",   count: 1}
  // => ]
  app.post("/api/v1/counter/inc", function(req, res) {
    res.json(Counters.inc(req.body.id));
  });
  
  // [json] POST {id: "zxcv"} /api/v1/counter/dec
  // => [
  // =>   {id: "zxcv", title: "steve", count: 2},
  // =>   {id: "qwer", title: "bob",   count: 1}
  // => ]
  app.post("/api/v1/counter/dec", function(req, res) {
    res.json(Counters.dec(req.body.id));
  });
  
  app.get("*", sendFile("index.html"));
  app.head("*", sendFile("index.html"));
  
  app.listen(PORT, console.log.bind(null, "PORT: " + PORT));

  